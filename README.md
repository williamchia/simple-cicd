# simple-cicd

## Fork this repo

1. Click on "Fork"
1. Select a namespace to fork into

## Create a project

1. Crete a new Project - https://gitlab.com/projects/new
1. Select to "Create from template" tab
1. Select NodeJS
1. Select Public Visibility Level
1. Click on "Create project"


## Add Kubernetes Cluster (optional)

1. [Sign up for Google Cloud](#sign-up-for-google-cloud)
1. [Set up Kubernetes Cluster](#set-up-kubernetes-cluster)
1. [Set up Auto DevOps](#set-up-auto-devops)

### Sign up for Google Cloud

If you want to deploy your app today, you'll need a Kubernetes cluster. These optional instructions are to sign up for a GKE account. You can complete the build and test stages without signing up.

1. Visit https://cloud.google.com/
2. Click on "Get started for free"
3. Select country, accept terms, and continue
4. Add address & credit card (You start with $300 free credit)
5. Click "Start free trial"
6. Go to Compute Engine
7. Click "Enable billing" (setup takes ~10 min)

### Set up Kubernetes Cluster

1. Go to Operations > Kubernetes
1. Click "Add a Kubernetes cluster"
1. Click "Sign in with Google"
1. Choose your google account
1. Click "Allow"
1. Enter `simple-cicd` for the cluster name
1. Select a project (use "My first project")
1. Select a Zone `europe-north-a`
1. Set number of nodes to 1
1. Make sure "RBAC-enabled cluster" is checked
1. Click on "Create Kubernetes Cluster" (creating a cluster takes ~10 min)

### Install Managed Apps

1. Use the single-click install for the following apps
  - Helm Tiller (takes ~5 min, needs to install first)
  - Ingress
  - Prometheus
1. Wait for Ingress to complete and give an IP address (takes ~5 min, may need to refresh page.)
1. Add a base domain. Use <ip_address>.nip.io (use the IP from Ingress)
1. Click "Save changes"

### Set up Auto DevOps

1. Go to Settings > CI/CD
1. Expand Auto DevOps
1. Select Default to Auto DevOps pipeline
1. Choose "Continuous deployment to production"
1. Save changes

When you save your first pipeline should run!

## Create a custom pipeline

### build

1. Create a `.gitlab-ci.yaml` file
1. Add the following
```
image: node:latest

stages:
  - build

build-job:
  stage: build
  script:
    - npm install
```




based on https://gitlab.com/zh-examples/gitlabci-demo